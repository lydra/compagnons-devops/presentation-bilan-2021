# Présentation bilan 2021

Présentation du bilan 2021 des Compagnons du DevOps. Live du 07/01/2022 17h

Ce dépôt utilise [Slidev](https://github.com/slidevjs/slidev).
## Getting started

To start the slide show:

- `npm install`
- `npm run dev`
- visit http://localhost:3030

Edit the [slides.md](./slides.md) to see the changes.

Learn more about Slidev on [documentations](https://sli.dev/).

## Licence

**Copyright (C) 2022 Chaudier Christophe (Lydra)**

Cette présentation est sont sous [Licence Libre CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/deed.fr), vous pouvez la réutiliser en modifiant vos informations et en me créditant comme source.

[![licensebuttons cc-by-sa](https://licensebuttons.net/l/by-sa/3.0/88x31.png)](https://creativecommons.org/licenses/by-sa/4.0)
