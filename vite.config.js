// vite.config.js
// https://vitejs.dev/guide/static-deploy.html#gitlab-pages-and-gitlab-ci

const URL = process.env.BASE_URL || '/';

export default {
  base: `${URL}`
}