---
# try also 'default' to start simple
# https://sli.dev/themes/gallery.html
theme: default
# random image from a curated Unsplash collection by Anthony
# like them? see https://unsplash.com/collections/94734566/slidev
background: https://source.unsplash.com/collection/94734566/1920x1080
# apply any windi css classes to the current slide
class: 'text-center'
# https://sli.dev/custom/highlighters.html
highlighter: shiki
# show line numbers in code blocks
lineNumbers: false
# some information about the slides, markdown enabled
info: |
  ## Slidev Starter Template
  Presentation slides for developers.

  Learn more at [Sli.dev](https://sli.dev)
# persist drawings in exports and build
drawings:
  persist: false
layout: image
image: img/Cover_Live_Bilan_2021.png
---

<div class="abs-br m-6 flex gap-2">
  <button @click="$slidev.nav.openInEditor()" title="Open in Editor" class="text-xl icon-btn opacity-50 !border-none !hover:text-white">
    <carbon:edit />
  </button>
  <a href="https://gitlab.com/lydra/compagnons-devops/presentation-bilan-2021" target="_blank" alt="GitLab"
    class="text-xl icon-btn opacity-50 !border-none !hover:text-white">
    <fa-brands:gitlab />
  </a>
</div>

<!--
The last comment block of each slide will be treated as slide notes. It will be visible and editable in Presenter Mode along with the slide. [Read more in the docs](https://sli.dev/guide/syntax.html#notes)
-->

---
layout: image-right
image: https://source.unsplash.com/AT77Q0Njnt0
---

# Sommaire

<br/>
<br/>

- 👋 **Introduction**
- ⏲️ **Bilan 2021**
  - 📊 Les stats
  - 🤑 Le Budjet
  - 🤔 Mon retour
- 🔮 **La suite pour 2022**


<br/>
<br/>

🙏 Vous connaissez la chanson : **abonnez-vous, activer la cloche et mettez des pouces** 👍

---
layout: cover
---
# Bilan 2021 ⏲️
Retour sur une année pleine de nouveautés

---

# Le podcast

- Un seul épisode Solo
- Aide d'une monteuse en cours d'année
- Arrivé de 2 nouveaux pocasteurs
- Nous sommes 7
- Arrivée de la monétisation

---

## 📊 Les stats

[![](/img/stats_podcast.png)](https://insights.acast.com/shows/5df7c112b9e5bf6d176cbfcb?from=2020-12-31T23%3A00%3A00.000Z&interval=month&to=2021-12-31T22%3A59%3A59.999Z)

---

## Comparaison avec 2020

|     | 2020 | 2021 | Tendance |
| --- | ---- | ---- | ---- |
| Téléchargements | 23,8k | 23,5k | ➡️ |
| Épisodes | 41 | 21 | ↘️ |

<br/>
<br/>

> 48k téléchargements au total

<br/>

| Nom |  Téléchargements |
| --- | ---- |
| 🥇 RDO #1 - L’ingénieur DevOps mythe ou réalité ? | 1 588 |
| 🥈 En Solo #11 - Qu'est-ce qu'un Ingénieur DevOps, un SysOps ou AdminSys ? |	1 234 |
| 🥉 RDO #3 - Comment bien débuter l'infrastructure as code ?	|	1 095	|

---

# YouTube

- Arrivée des lives
  - Lives trimestriels
  - Libres Antennes
  - Rencontre virtuelles des Compagnons
  - Lives coding
- Un seul tuto
- Arrivée de la monétisation

---

## 📊 Les stats

[![](/img/stats_youtube.png)](https://studio.youtube.com/channel/UCauIDghddUNu6Fto1nR9Bmg/analytics/tab-overview/period-minus_1_year)

---

## Comparaison avec 2020

|     | 2020 | 2021 | Tendance |
| --- | ---- | ---- | ---- |
| Vues | 24,5k | 36,8k | ↗️ |
| Abonnés | 2,1k | 1,2k | ↘️ |
| Lives | 0 | 19 | ↗️ |
| Vidéo | 81 | 68 | ↘️ |

<br/>
<br/>

> 62k vues au total

<br/>

| Nom |  Téléchargements |
| --- | ---- |
| 🥇 Comment faire une revue de code ? - TUTO - GitLab | 2 223 |
| 🥈 Comment configurer et sécuriser son dépôt GitLab ? - TUTO - GitLab | 2 071 |
| 🥉 Comment migrer de Github à GitLab ?	- TUTO - GitLab |	1 926	|

---

# Les compagnons
## 📊 Les stats

[![](/img/stats_forum.png)](https://forum.compagnons-devops.fr/admin?period=yearly)

---

## Comparaison avec 2020

|     | 2020 | 2021 | Tendance |
| --- | ---- | ---- | ---- |
| Connections | 79,4k | 97,7k | ↗️ |
| Posts | 2,6 k | 2,7k | ➡️ |
| Inscrits | 403 | 281 | ↘️ |

<br/>
<br/>

> 937/1207 inscrits au total


---

# 🤑 Le Budjet

## Dépences

| Quoi ?    | Combien ? |
| --- | ---- |
| Hébergement Podcast (Acast) | 141€ |
| Enregistrement (Riverside) | 171€ |
| Montage | 510€ |
| Infrastructure | 215€ |
| Matériel | 460€ |
| **TOTAL** | **1 497€** |

---

# 🤑 Le Budjet

## Recettes

| Quoi ?    | Combien ? |
| --- | ---- |
| Partenaria | 0€ |
| Affiliation | 0€ |
| Publicité | 0€ |
| Dons | 150€ |
| Vente formation (5) | 171€ |
| **TOTAL** | **725€** |

## Résultat −772€ 😅

---

# ⏱️ Le temps

| Qui | Temps (h) |
| --- | ---- |
| Christophe | 403 |
| Assistante | 64 |
| Thomas | 1 |
| **TOTAL** | 468 |

---

# 🤔 Mon retour

## Flops ⬇️
- Année en retrait
- Formation ❌
- Pas de suivis des stats

## Top ⬆️
- Renforcement de la communauté
- Gain d'experience
- Image de marque

---
layout: cover
---
# La suite pour 2022 🔮
Qu'est-ce que je vous reserve ?

---

# YouTube

## La chaîne
- Retour des TUTO
- Activation de l'espace membre et des superchats

## Lives
- Live mensuel tous les 1er vendredis à 17h
- Plus de Libre Antenne (1 par trimestre)

---

# Les Compagnons du DevOps

- Arrivé des modérateurs
- Changement de serveurs
- Arrivé du wiki


---

# Les services

- Une formation par trimestre
- Lancement des mentorats d'équipes
- Lancement des mentorats de Groupe GitLab
- Lancement de l'offre [Mentor DevOps as a service](https://forum.compagnons-devops.fr/t/je-me-questionne-sur-une-offre-mentor-devops-as-a-service/1235)



---
layout: center
class: text-center
---

# ❔ Vos Questions ❔

<br/>
<br/>

## Crédits

[**Sli**dev](https://sli.dev) · [Unslpash](https://unsplash.com/) · [CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)

## Pour me soutenir

💖 Tu peu soutenir mon travail et la communauté sur [Liberapay](https://liberapay.com/cchaudier)
